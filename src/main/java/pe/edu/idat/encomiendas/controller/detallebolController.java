package pe.edu.idat.encomiendas.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import pe.edu.idat.encomiendas.service.detallebolService;
import pe.edu.idat.encomiendas.util.Constantes;
import pe.edu.idat.encomiendas.bean.detalleboletaBean;

@Controller
@RequestMapping({"Detalle", "Det" })
public class detallebolController {	
	// DECLARACIONES CONTROLLER
	@Autowired		
detallebolService detalleService;

	boolean result;
	
	// METODO GET MOSTRAR WEB -> MODALANDVIEW
	@RequestMapping(value="/Web", method = RequestMethod.GET)
	public ModelAndView showView(HttpServletRequest request, HttpServletResponse response) {
		ModelAndView modelo = new ModelAndView();
		try {
			// MODALANDVIEW JSP
			modelo.setViewName("viewdetalleboleta");
			
			// LISTAR USUARIOS 		
			List<detalleboletaBean> lstdetbol =detalleService.listardetalleboleta();
			
			// AGREGAR LISTAS AL MODALANDVIEW
			modelo.addObject("listadodetalleboleta", lstdetbol);
			
		} catch (Exception e) {
			// CONTROL ERROR 500
			modelo.setViewName("view500");
			modelo.addObject(Constantes.MESSAGE_TEXT,e.getLocalizedMessage());
		}
		return modelo;
	}
		
}