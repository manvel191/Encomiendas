package pe.edu.idat.encomiendas.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.edu.idat.encomiendas.bean.detalleboletaBean;
import pe.edu.idat.encomiendas.dao.detallebolDaoImpl;

@Service("detallebolService")
public class detallebolServiceImpl implements detallebolService {
	// DECLARACIONES SERVICE
	private static final Log log = LogFactory.getLog(detallebolServiceImpl.class);	
	private boolean result;
	detalleboletaBean detalleboletaBean;	
	
	@Autowired
	private detallebolDaoImpl detallebolDao;	
	
	// METODO LISTAR TODOS
	@Override
	public List<detalleboletaBean> listardetalleboleta() throws Exception {
		if (log.isDebugEnabled()) log.debug("Inicio - detallebolServiceImpl.listardetalleboleta");
		List<detalleboletaBean> lst = new ArrayList<>();	
		try {
			// LISTAR USUARIOS
			lst = detallebolDao.seldetalleboleta();
			
		}catch(Exception ex) {
			if (log.isDebugEnabled()) log.debug("Error -  detallebolServiceImpl.listardetalleboleta");
			log.error(ex, ex);
			throw ex;
		}finally{
			if (log.isDebugEnabled()) log.debug("Final -  detallebolServiceImpl.listardetalleboleta");
		} 	
		return lst;
	}
	
	
}