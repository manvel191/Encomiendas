package pe.edu.idat.encomiendas.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;

import pe.edu.idat.encomiendas.dao.detallebolDaoImpl;

import pe.edu.idat.encomiendas.bean.detalleboletaBean;

public class detallebolDaoImpl implements detallebolDao{

	static Log log = LogFactory.getLog(detallebolDaoImpl.class.getName());	
	private JdbcTemplate jdbcTemplate;
    private int rows;
    private detalleboletaBean detalleboletaBean = null;
    
	// JDBCTEMPLATE 
    public void setJdbcTemplate(JdbcTemplate jdbcTemplate){
        this.jdbcTemplate = jdbcTemplate;
    }
	
		
	@Override
	public List<detalleboletaBean> seldetalleboleta() throws Exception {

		if (log.isDebugEnabled()) log.debug("Inicio - detallebolDaoImpl.selectdetallebol");
		
		List<detalleboletaBean> lstdetbol = null;
		String sql ="SELECT * FROM det_boleta";
		
		try {		
			lstdetbol = jdbcTemplate.query(sql, new ResultSetExtractor<List<detalleboletaBean>>(){
	            @Override
	            public List<detalleboletaBean> extractData(ResultSet rs) throws SQLException
	            {
	            	List<detalleboletaBean> list = new ArrayList<>();
	                while (rs.next())
	                {
	                	detalleboletaBean d = new detalleboletaBean();
	                	d.setIddetboleta(rs.getString("det_iddetboleta"));
	                	d.setTipopago(rs.getString("det_tipopago"));	                          	
	                    list.add(d);
	                }
	                return list;
	            }

	        });	    
		
		} catch (Exception ex) {			
			if (log.isDebugEnabled()) log.debug("Error - detallebolDaoImpl.selectdetallebol");
			log.error(ex, ex);
			throw ex;
		}finally{
			if (log.isDebugEnabled()) log.debug("Final - detallebolDaoImpl.selectdetallebol");
		}
	    return lstdetbol;
	

	
	}
	
}