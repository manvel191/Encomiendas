﻿# Host: localhost  (Version 5.7.21)
# Date: 2018-05-03 12:54:57
# Generator: MySQL-Front 6.0  (Build 2.20)


#
# Structure for table "boleta"
#

DROP TABLE IF EXISTS `boleta`;
CREATE TABLE `boleta` (
  `idboleta` int(11) NOT NULL,
  `fecha` datetime NOT NULL,
  `monto` decimal(10,2) NOT NULL,
  PRIMARY KEY (`idboleta`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

#
# Data for table "boleta"
#

/*!40000 ALTER TABLE `boleta` DISABLE KEYS */;
INSERT INTO `boleta` VALUES (1,'2017-09-12 00:00:00',5.20),(2,'2018-03-17 00:00:00',10.30);
/*!40000 ALTER TABLE `boleta` ENABLE KEYS */;

#
# Structure for table "destinatario"
#

DROP TABLE IF EXISTS `destinatario`;
CREATE TABLE `destinatario` (
  `iddestinatario` int(11) NOT NULL,
  `nombre` varchar(20) NOT NULL,
  `apellidos` varchar(30) NOT NULL,
  `telefono` char(15) NOT NULL,
  `direccion` varchar(30) NOT NULL,
  `dni` char(8) NOT NULL,
  PRIMARY KEY (`iddestinatario`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

#
# Data for table "destinatario"
#

/*!40000 ALTER TABLE `destinatario` DISABLE KEYS */;
INSERT INTO `destinatario` VALUES (1,'Alonso','Paico Pisconti','912312312','Amauta 132 Surco','7655665'),(2,'Abel','Heredia Bravo','912121212','Jr.Union 131 ','7121212');
/*!40000 ALTER TABLE `destinatario` ENABLE KEYS */;

#
# Structure for table "destino"
#

DROP TABLE IF EXISTS `destino`;
CREATE TABLE `destino` (
  `iddestino` int(11) NOT NULL,
  `provincia` varchar(20) NOT NULL,
  `distrito` varchar(20) NOT NULL,
  `direccion` varchar(30) NOT NULL,
  PRIMARY KEY (`iddestino`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

#
# Data for table "destino"
#

/*!40000 ALTER TABLE `destino` DISABLE KEYS */;
INSERT INTO `destino` VALUES (1,'Puno','jajapallca','Jr.Los Cactus'),(2,'Huancavelica','chanchasapo','Jr.Las Concordias');
/*!40000 ALTER TABLE `destino` ENABLE KEYS */;

#
# Structure for table "det_boleta"
#

DROP TABLE IF EXISTS `det_boleta`;
CREATE TABLE `det_boleta` (
  `iddetboleta` int(11) NOT NULL,
  `tipopago` varchar(20) NOT NULL,
  PRIMARY KEY (`iddetboleta`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

#
# Data for table "det_boleta"
#

/*!40000 ALTER TABLE `det_boleta` DISABLE KEYS */;
INSERT INTO `det_boleta` VALUES (1,'soles'),(2,'soles');
/*!40000 ALTER TABLE `det_boleta` ENABLE KEYS */;

#
# Structure for table "det_encomienda"
#

DROP TABLE IF EXISTS `det_encomienda`;
CREATE TABLE `det_encomienda` (
  `iddet_encomienda` int(11) NOT NULL,
  `descripcion` varchar(100) NOT NULL,
  `peso` char(10) NOT NULL,
  `tamaño` char(10) NOT NULL,
  `estado` varchar(20) NOT NULL,
  PRIMARY KEY (`iddet_encomienda`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

#
# Data for table "det_encomienda"
#

/*!40000 ALTER TABLE `det_encomienda` DISABLE KEYS */;
INSERT INTO `det_encomienda` VALUES (1,'refrigeradora','20kg','60x70cm','activo'),(2,'bicicleta','10kg','20x10cm','activo');
/*!40000 ALTER TABLE `det_encomienda` ENABLE KEYS */;

#
# Structure for table "encomiendas"
#

DROP TABLE IF EXISTS `encomiendas`;
CREATE TABLE `encomiendas` (
  `idencomiendas` int(11) NOT NULL,
  `destino` varchar(20) NOT NULL,
  `fechasalida` datetime NOT NULL,
  `fechaentrega` datetime NOT NULL,
  `propietario` varchar(20) NOT NULL,
  `estadoenvio` varchar(50) NOT NULL,
  PRIMARY KEY (`idencomiendas`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

#
# Data for table "encomiendas"
#

/*!40000 ALTER TABLE `encomiendas` DISABLE KEYS */;
INSERT INTO `encomiendas` VALUES (1,'huancayo','2013-12-17 00:00:00','2014-12-17 00:00:00','Saul','Fragil'),(2,'amazonas','2015-12-17 00:00:00','2018-12-17 00:00:00','Mallco','No Fragil');
/*!40000 ALTER TABLE `encomiendas` ENABLE KEYS */;

#
# Structure for table "propietario"
#

DROP TABLE IF EXISTS `propietario`;
CREATE TABLE `propietario` (
  `idpropietario` int(11) NOT NULL,
  `nombre` varchar(20) NOT NULL,
  `apellidos` varchar(30) NOT NULL,
  `telefono` char(15) NOT NULL,
  `direccion` varchar(30) NOT NULL,
  `dni` char(8) NOT NULL,
  PRIMARY KEY (`idpropietario`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

#
# Data for table "propietario"
#

/*!40000 ALTER TABLE `propietario` DISABLE KEYS */;
INSERT INTO `propietario` VALUES (1,'Felipe','Vega Yarleque','987654321','Av.Azuzenas 212 Miraflores','7681234'),(2,'Rodrigo','Quispe Mamani','91345678','Av.Macarena 121 V.M.T','7677234');
/*!40000 ALTER TABLE `propietario` ENABLE KEYS */;
